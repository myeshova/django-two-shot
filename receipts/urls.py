from receipts.views import receipt_list
from django.urls import path

urlpatterns = [
path("", receipt_list, name="home")
]
